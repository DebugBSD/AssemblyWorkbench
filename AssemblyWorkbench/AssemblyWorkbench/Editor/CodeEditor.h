#ifndef CODEEDITOR_H
#define CODEEDITOR_H

#include <QWidget>
#include <QTextEdit>
#include <QTextDocument>
#include <QObject>
#include <QUrl>
#include <QFileDialog>
#include <QFile>
#include <QTextStream>

#include "linenumberarea.h"
#include "Highlighter.h"
#include "Container/Document.h"

class QPaintEvent;
class QResizeEvent;
class QSize;
class QWidget;
class LineNumberArea;

class CodeEditor : public QWidget
{
    Q_OBJECT

public:
    CodeEditor(QWidget *parent = 0);

//    void lineNumberAreaPaintEvent(QPaintEvent *event);
//    int lineNumberAreaWidth();

    bool getModified() const { return m_isModified; }
    void setModified(bool isModified = true) { m_isModified = isModified; }

    bool saveFile();

    bool requestToSave();
    QUrl getUrlFilePath() const { return m_urlFilePath; }
    void setUrlFilePath(const QUrl &urlFilePath) { m_urlFilePath = urlFilePath; }

    void openFile(QUrl &fileName);

public slots:
    void textChanged();

protected:
    void resizeEvent(QResizeEvent *event) override;
    void closeEvent(QCloseEvent *event) override;

private slots:
//    void updateLineNumberAreaWidth(int newBlockCount);
//    void highlightCurrentLine();
//    void updateLineNumberArea(const QRect &, int);



private:
    QWidget *lineNumberArea;
    QTextEdit *m_editor;

//    Document *m_pDocument;
    Highlighter *m_pHighlighter;

    bool m_isModified;

    QUrl m_urlFilePath; // Full path to file: C:/example/filename.asm.
};

#endif

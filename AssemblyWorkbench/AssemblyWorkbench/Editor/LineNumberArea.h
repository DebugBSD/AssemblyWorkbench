#ifndef LINENUMBERAREA_H
#define LINENUMBERAREA_H

#include <QWidget>
#include <QTextEdit>
#include <QPaintEvent>

class LineNumberArea : public QWidget
{
    Q_OBJECT
public:
    explicit LineNumberArea(QTextEdit *parent = nullptr);
//    QSize sizeHint() const;
signals:

public slots:

protected:
//    void paintEvent(QPaintEvent *e);

private:
    QTextEdit *codeEditor;
};

#endif // LINENUMBERAREA_H

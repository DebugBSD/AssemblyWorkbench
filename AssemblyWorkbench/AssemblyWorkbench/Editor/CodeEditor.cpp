#include <QtWidgets>

#include "codeeditor.h"


CodeEditor::CodeEditor(QWidget *parent) : QWidget (parent)
{
    m_editor = new QTextEdit(this);
    lineNumberArea = new LineNumberArea(m_editor);
    // Highlihgter
    QFont font;
    font.setFamily("Courier");
    font.setFixedPitch(true);
    font.setPointSize(10);

    setFont(font);
    m_pHighlighter = new Highlighter(m_editor->document());

//    setWindowTitle("New file");
//    setDocumentTitle("New File");

//    connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumberAreaWidth(int)));
//    connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumberArea(QRect,int)));
//    connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(highlightCurrentLine()));
//    connect(this, SIGNAL(textChanged()), this, SLOT(textChanged()));

//    updateLineNumberAreaWidth(0);
//    highlightCurrentLine();

}

void CodeEditor::textChanged()
{
    setModified(true);
}

//int CodeEditor::lineNumberAreaWidth()
//{
//    int digits = 1;
//    int max = qMax(1, m_pDocument->blockCount());
//    while (max >= 10) {
//        max /= 10;
//        ++digits;
//    }

//    int space = 10 + fontMetrics().width(QLatin1Char('9')) * digits;
//    return space;
//}

bool CodeEditor::requestToSave()
{
    if(getModified())
    {
        if(m_urlFilePath.isEmpty())
        {
            QFileDialog *pFileDialog = new QFileDialog();
            pFileDialog->setAcceptMode(QFileDialog::AcceptSave);
            pFileDialog->setFileMode(QFileDialog::AnyFile);
            if(pFileDialog->exec() == QDialog::Accepted)
            {
                if(pFileDialog->selectedUrls().length() == 1)
                {
                    m_urlFilePath = pFileDialog->selectedUrls().at(0);
                    saveFile();
                }
            }
        }
        else
        {
            saveFile();
        }
    }

    return true;
}

void CodeEditor::openFile(QUrl &fileName)
{
    setUrlFilePath(fileName);

    QFile f{m_urlFilePath.toLocalFile()};
    if(f.open(QIODevice::ReadOnly))
    {
//        setPlainText(f.readAll());
        f.close();

//        setWindowTitle(m_urlFilePath.fileName());
        setModified(false);
    }


}

bool CodeEditor::saveFile()
{
    QFile f{m_urlFilePath.toLocalFile()};
    if(f.open(QIODevice::Truncate | QIODevice::WriteOnly))
    {
        QTextStream s(&f);
//        s << toPlainText();
        f.close();

//        setWindowTitle(m_urlFilePath.fileName());
        setModified(false);
    }
    return true;
}



//void CodeEditor::updateLineNumberAreaWidth(int /* newBlockCount */)
//{
////    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
//}



//void CodeEditor::updateLineNumberArea(const QRect &rect, int dy)
//{

////    if (dy)
////        lineNumberArea->scroll(0, dy);
////    else
////        lineNumberArea->update(0, rect.y(), lineNumberArea->width(), rect.height());

////    if (rect.contains(viewport()->rect()))
////        updateLineNumberAreaWidth(0);
//}

void CodeEditor::resizeEvent(QResizeEvent *e)
{
    m_editor->resize(e->size().width(), e->size().height());

//    QRect cr = contentsRect();
//    lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

void CodeEditor::closeEvent(QCloseEvent *event)
{
    if(getModified())
    {
        // We asks if we want to save it before close it.
        QMessageBox msgBox;
        msgBox.setText("This file has been modified.");
        msgBox.setInformativeText("Do you want to save it?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard);
        msgBox.setDefaultButton(QMessageBox::Save);
        if(msgBox.exec() == QMessageBox::Save)
        {
            qDebug() << "Saving changes...";
            requestToSave();
        }
    }
}



//void CodeEditor::highlightCurrentLine()
//{
//    QList<QTextEdit::ExtraSelection> extraSelections;

//    if (m_editor->isReadOnly()) {
//        QTextEdit::ExtraSelection selection;

//        QColor lineColor = QColor(Qt::yellow).lighter(160);

//        selection.format.setBackground(lineColor);
//        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
//        selection.cursor = m_editor->textCursor();
//        selection.cursor.clearSelection();
//        extraSelections.append(selection);
//    }

//    m_editor->setExtraSelections(extraSelections);
//}



//void CodeEditor::lineNumberAreaPaintEvent(QPaintEvent *event)
//{
//    QPainter painter(lineNumberArea);
//    painter.fillRect(event->rect(), Qt::lightGray);


//    QTextBlock block = m_editor->firstVisibleBlock();
//    int blockNumber = block.blockNumber();
//    int top = (int) m_editor->blockBoundingGeometry(block).translated(contentOffset()).top();
//    int bottom = top + (int) blockBoundingRect(block).height();

//    while (block.isValid() && top <= event->rect().bottom()) {
//        if (block.isVisible() && bottom >= event->rect().top()) {
//            QString number = QString::number(blockNumber + 1);
//            painter.setPen(Qt::black);
//            painter.drawText(0, top, lineNumberArea->width(), fontMetrics().height(),
//                             Qt::AlignRight, number);
//        }

//        block = block.next();
//        top = bottom;
//        bottom = top + (int) blockBoundingRect(block).height();
//        ++blockNumber;
//    }
//}

#include "MainWindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    resize(QSize(1024,768));

    m_menuBar = menuBar();

    m_statusBar = statusBar();

    connect(m_menuBar, &QMenuBar::triggered, this, &MainWindow::openActionHnld);

    createMenus();

    m_workSpace = new QMdiArea();
    m_workSpace->setViewMode(QMdiArea::TabbedView);
    m_workSpace->setTabsClosable(true);
    m_workSpace->setTabsMovable(true);

    setCentralWidget(m_workSpace);
}

void MainWindow::openActionHnld(QAction *pAction)
{
    if(pAction != nullptr)
    {
        // New Document
        qDebug() << "Action issued: " << pAction->text();
        if(pAction->text() == NEW_FILE_MENU)
        {
            CodeEditor *codeEditor = new CodeEditor();

            QMdiSubWindow* sw = new QMdiSubWindow( m_workSpace );
            // En este widget puedes meter cualquier cosa...
            sw->setWidget( codeEditor );
            sw->setAttribute( Qt::WA_DeleteOnClose );
            m_workSpace->addSubWindow( sw );
            sw->show();
        }
        // New project with one document by default
        else if(pAction->text() == NEW_PROJECT_MENU)
        {

        }
        else if(pAction->text() == OPEN_PROJECT_MENU)
        {

        }
        else if(pAction->text() == SAVE_PROJECT_MENU)
        {

        }
        else if(pAction->text() == SAVE_PROJECT_AS_MENU)
        {

        }
        else if(pAction->text() == SAVE_AS_TEMPLATE_MENU)
        {

        }
        else if(pAction->text() == OPEN_MENU)
        {
            QFileDialog *pFileDialog = new QFileDialog();
            pFileDialog->setAcceptMode(QFileDialog::AcceptOpen);
            pFileDialog->setFileMode(QFileDialog::ExistingFile);
            if(pFileDialog->exec() == QDialog::Accepted)
            {
                if(pFileDialog->selectedUrls().length() == 1)
                {
                    CodeEditor *pCodeEditor = new CodeEditor();
                    QUrl file = pFileDialog->selectedUrls().at(0);
                    pCodeEditor->openFile(file);

                    QMdiSubWindow* sw = new QMdiSubWindow( m_workSpace );
                    // En este widget puedes meter cualquier cosa...
                    sw->setWidget( pCodeEditor );
                    sw->setAttribute( Qt::WA_DeleteOnClose );
                    m_workSpace->addSubWindow( sw );
                    sw->show();
                }
            }
        }
        else if(pAction->text() == CLOSE_MENU)
        {

        }
        else if(pAction->text() == SAVE_MENU)
        {

            if(m_workSpace->currentSubWindow() != nullptr)
            {
                CodeEditor *pCodeEditor = (CodeEditor *)m_workSpace->currentSubWindow()->widget();
                pCodeEditor->requestToSave();
            }

        }
        else if(pAction->text() == SAVE_ALL_MENU)
        {

        }
        else if(pAction->text() == RECENT_MENU)
        {

        }
        else if(pAction->text() == EXIT_MENU)
        {
            // Check for modified files and ask for save them.
            exit(EXIT_SUCCESS);
        }
        else if(pAction->text() == COPY_MENU)
        {

        }
        else if(pAction->text() == PASTE_MENU)
        {

        }
        else if(pAction->text() == SELECT_ALL_MENU)
        {

        }
        else if(pAction->text() == BUILD_ALL_MENU)
        {

        }
        else if(pAction->text() == REBUILD_ALL_MENU)
        {

        }
        else if(pAction->text() == CLEAN_MENU)
        {

        }
        else if(pAction->text() == BUILD_OPTIONS_MENU)
        {

        }
        else if(pAction->text() == DISASSEMBLY_MENU)
        {

        }
        else
        {
            qDebug() << "Unknown menu";
        }
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
  {
//      if (maybeSave()) {
//          writeSettings();
//          event->accept();
//      } else {
//          event->ignore();
//      }

    qDebug() << "Close evenet emitido...";
}

void MainWindow::createMenus()
{
    m_pToolBar = addToolBar("File");

    m_fileAction = m_menuBar->addMenu(FILE_MENU);
    m_editAction = m_menuBar->addMenu(EDIT_MENU);
    m_buildAction = m_menuBar->addMenu(BUILD_MENU);
    m_toolsAction = m_menuBar->addMenu(TOOLS_MENU);

    m_fileAction->addAction(NEW_PROJECT_MENU);
    m_fileAction->addAction(OPEN_PROJECT_MENU);
    m_fileAction->addAction(SAVE_PROJECT_MENU);
    m_fileAction->addAction(SAVE_PROJECT_AS_MENU);
    m_fileAction->addAction(SAVE_AS_TEMPLATE_MENU);
    m_fileAction->addSeparator();

    // New file
    const QIcon newIcon = QIcon::fromTheme("document-new",QIcon(":/icons/32x32/actions/document-new.png"));
    QAction *newFileAction = new QAction(newIcon,NEW_FILE_MENU,this);
    m_fileAction->addAction(newFileAction);
    m_pToolBar->addAction(newFileAction);

    // Open file
    const QIcon openIcon = QIcon::fromTheme("document-open",QIcon(":/icons/32x32/actions/document-open.png"));
    QAction *newOpenFileAction = new QAction(openIcon,OPEN_MENU,this);
    m_fileAction->addAction(newOpenFileAction);
    m_pToolBar->addAction(newOpenFileAction);

    // Close file
    const QIcon closeIcon = QIcon::fromTheme("document-close",QIcon(":/icons/32x32/actions/document-close.png"));
    QAction *closeFileAction = new QAction(closeIcon,CLOSE_MENU,this);
    m_fileAction->addAction(closeFileAction);
    m_pToolBar->addAction(closeFileAction);

    // Save file
    const QIcon saveIcon = QIcon::fromTheme("document-close",QIcon(":/icons/32x32/actions/document-save.png"));
    QAction *saveFileAction = new QAction(saveIcon,SAVE_MENU,this);
    m_fileAction->addAction(saveFileAction);
    m_pToolBar->addAction(saveFileAction);

    // Save all
    const QIcon saveAllIcon = QIcon::fromTheme("document-close",QIcon(":/icons/32x32/actions/document-save.png"));
    QAction *saveAllFileAction = new QAction(saveAllIcon,SAVE_ALL_MENU,this);
    m_fileAction->addAction(saveAllFileAction);
    m_pToolBar->addAction(saveAllFileAction);


    m_fileAction->addSeparator();
    m_fileAction->addAction(RECENT_MENU);
    m_fileAction->addSeparator();
    m_fileAction->addAction(EXIT_MENU);

    m_editAction->addAction(COPY_MENU);
    m_editAction->addAction(PASTE_MENU);
    m_editAction->addAction(SELECT_ALL_MENU);

    m_buildAction->addAction(BUILD_ALL_MENU);
    m_buildAction->addAction(REBUILD_ALL_MENU);
    m_buildAction->addAction(CLEAN_MENU);
    m_buildAction->addAction(BUILD_OPTIONS_MENU);
    m_buildAction->addAction(DISASSEMBLY_MENU);
}

void MainWindow::createToolBars()
{

}

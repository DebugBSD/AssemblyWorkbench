#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QApplication>
#include <QMainWindow>
#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QMenu>
#include <QAction>
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QTextEdit>
#include <QIcon>
#include <QCloseEvent>
#include <QList>

#include "Container/Document.h"
#include "Container/Project.h"
#include "Editor/CodeEditor.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);

signals:

public slots:
    void openActionHnld(QAction *pAction = nullptr);
    void closeEvent(QCloseEvent * event);

private:
    // Constants for menu's
    static constexpr const char *FILE_MENU = "File";
    static constexpr const char *EDIT_MENU = "Edit";
    static constexpr const char *BUILD_MENU = "Build";
    static constexpr const char *TOOLS_MENU = "Tools";

    static constexpr const char *NEW_PROJECT_MENU = "New Project";
    static constexpr const char *OPEN_PROJECT_MENU = "Open Project";
    static constexpr const char *SAVE_PROJECT_MENU = "Save Project";
    static constexpr const char *SAVE_PROJECT_AS_MENU = "Save Project As";
    static constexpr const char *SAVE_AS_TEMPLATE_MENU = "Save Project as Template";
    static constexpr const char *NEW_FILE_MENU = "New File";
    static constexpr const char *OPEN_MENU = "Open File";
    static constexpr const char *CLOSE_MENU = "Close File";
    static constexpr const char *SAVE_MENU = "Save File";
    static constexpr const char *SAVE_ALL_MENU = "Save All";
    static constexpr const char *RECENT_MENU = "Recent";
    static constexpr const char *EXIT_MENU = "Exit";

    static constexpr const char *COPY_MENU = "Copy";
    static constexpr const char *PASTE_MENU = "Paste";
    static constexpr const char *SELECT_ALL_MENU = "Select All";

    static constexpr const char *BUILD_ALL_MENU = "Build Project";
    static constexpr const char *REBUILD_ALL_MENU = "Rebuild Project";
    static constexpr const char *CLEAN_MENU = "Clean Project";
    static constexpr const char *BUILD_OPTIONS_MENU = "Build Options";
    static constexpr const char *DISASSEMBLY_MENU = "Disassembly";

private: // Private methods
    void createMenus();
    void createToolBars();

private:
    QMenuBar *m_menuBar;
    QToolBar *m_pToolBar;
    QStatusBar *m_statusBar;

    QMenu *m_fileAction;
    QMenu *m_editAction;
    QMenu *m_toolsAction;
    QMenu *m_buildAction;

    QMdiArea *m_workSpace;

    QList<Document *> m_lDocuments;
    QList<Project *> m_lProjects;
};

#endif // MAINWINDOW_H

#ifndef PROJECT_H
#define PROJECT_H

#include <QList>

#include "Document.h"

class Project
{
public:
    Project();

private:
    QList<Document *>m_lDocuments;
};

#endif // PROJECT_H

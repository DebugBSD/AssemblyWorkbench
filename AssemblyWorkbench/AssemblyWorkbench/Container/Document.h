#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <QTextDocument>

class Document: public QTextDocument
{
public:
    Document(QTextDocument *parent);
};

#endif // DOCUMENT_H

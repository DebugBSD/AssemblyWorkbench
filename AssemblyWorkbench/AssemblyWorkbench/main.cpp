#include <QApplication>
#include <QMainWindow>
#include "MainWindow.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setAttribute(Qt::AA_NativeWindows);
    MainWindow *testWindow = new MainWindow();

    testWindow->show();

    return a.exec();
}

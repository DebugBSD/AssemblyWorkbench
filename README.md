#TODO

- Project Explorer.
- Syntax Highlight.
- Line number.
- Edition in multiple tabs.
- Find in files / Regex search and replace
- Disassembly binary data.
- Hexadecimal view.
- Integration with Debugger.
- Integration with Git.
- Tooltip with details of the symbol (variable, function, macro, data structure)
- Symbol Viewer.
- Support for Code Folding.
- Documentation generation (in HTML, man, ...)
- Customization of the environment.
	- Settings
		- When opening the editor, it has to show the las documents which were open before.
		
- Menus 
	- New File - Creates a new CodeEditor window empty.
	- Save File - Saves the current file. 
	- Open File - Open a file into a new CodeEditor window.
	- Close File - Closes the current file. It asks for save if current CodeEditor Window is not empty.
	- Save all - Save all files. 